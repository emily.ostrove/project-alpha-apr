from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy, reverse
from tasks.models import Task

# from django.shortcuts import render
# Create your views here.


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "tasks/new.html"
    fields = ["name", "start_date", "due_date", "project", "assignee"]
    success_url = reverse_lazy("list_projects")

    # a way to think about this: args=[self.object.(the field name of
    # what I'm trying to pull up).id])
    def get_success_url(self):
        return reverse("show_project", kwargs={"pk": self.object.project_id})

    # alternative solution is args bc we only need to pass 1 variable in there
    # return reverse("show_project", args=[self.object.project.id])


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/list.html"
    fields = ["name", "assignee", "start_date", "end_date", "is_completed"]

    # this filters the results you get to the results of the assignee
    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


class TaskUpdateView(UpdateView):
    model = Task
    fields = ["is_completed"]
    success_url = reverse_lazy("show_my_tasks")
