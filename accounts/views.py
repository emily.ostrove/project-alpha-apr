from django.shortcuts import redirect, render
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login

# Create your views here.


def signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            usernameAPP = request.POST.get("username")
            passwordAPP = request.POST.get("password1")
            # User is a model with a function create_user
            # setting our variables usernameAPP and passwordAPP
            # to the create_user parameters
            user = User.objects.create_user(
                username=usernameAPP, password=passwordAPP
            )
            user.save()
            login(request, user)
            return redirect("home")
    else:
        form = UserCreationForm(request.POST)
    context = {"form": form}
    return render(request, "registration/signup.html", context)
