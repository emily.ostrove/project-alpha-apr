from django.shortcuts import redirect
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from django.views.generic.detail import DetailView
from django.contrib.auth.mixins import LoginRequiredMixin


from projects.models import Project
from django.urls import reverse_lazy, reverse


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/list.html"
    success_url = reverse_lazy("home")

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.members = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("home")


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "projects/detail.html"
    fields = ["name", "description"]


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/new.html"
    fields = ["name", "description", "members"]

    def get_success_url(self):
        return reverse("show_project", kwargs={"pk": self.object.pk})

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super(ProjectCreateView, self).get_form_kwargs(
            *args, **kwargs
        )
        return kwargs
